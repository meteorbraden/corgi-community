<?php

	// Setup
	// -----
	$message = '';
	
	// (1) Check for update
	// --------------------
	if(isset($_POST) && !empty($_POST))
	{	
		
		$sql = "UPDATE settings SET `value` = CASE `name` ";
		
		foreach($_POST as $i => $v)
			$sql .= " WHEN '" . $db->real_escape_string($i) . "' THEN '" . $db->real_escape_string($v) . "'";
		
		$sql .= " ELSE `name` END";
		
		$success = data_query($sql);
		
		if($success)
			$message = '<div class="alert alert-success" role="alert">Awesome! Now punch yourself.</div>';
		else
			$message = '<div class="alert alert-danger" role="alert">Well that didn\'t work</div>';
	}
	
	// (2) Get settings
	// ----------------
	$settings = get_site_settings();
	
	echo $message;
?>


<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

	<?php foreach($settings as $setting)
		
		{
			$name = ucwords(str_replace('_', ' ', $setting['name']));
		?>
		<div class="col-lg-6">
			<div class="form-group">
				<label for="exampleInputPassword1"><?php echo $name; ?></label>
				<input class="form-control" type="text" name="<?php echo $setting['name']; ?>" value="<?php echo $setting['value']; ?>" />
			</div>
		</div>
	<?php } ?>
	
	<div class="col-lg-12">
		<div class="form-group" style="text-align: right;"><input class="btn btn-default" type="submit" value="Update Site Settings" /></div>
	</div>
</form>

<?php

	// Setup
	// -----
	$message = '';
	
	// (1) Check for update
	// --------------------
	if(isset($_POST['question']) && isset($_POST['answer']) && $_POST['question'] != '' && $_POST['answer'] != '')
	{	
		include_module('faq');
		
		$query = "INSERT INTO faqs (question, answer) VALUES ('" . $db->real_escape_string($_POST['question']) . "','" . $db->real_escape_string($_POST['answer']) . "');";
		$success = $db->query($query) or die('Error in the consult..' . mysqli_error($db));
		
		if($success)
			$message = '<div class="alert alert-success" role="alert">Oh goody, another person in here screwing things up. Add another one!</div>';
		else
			$message = '<div class="alert alert-danger" role="alert">Well that didn\'t work</div>';
	}
	
	echo $message;
?>


<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">


		<div class="col-lg-12">
			<div class="form-group">
				<label for="exampleInputPassword1">Title</label>
				<input class="form-control" type="text" name="title" value="" required="required" />
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">Link</label>
				<input class="form-control" type="text" name="link" value="" required="required" />
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">Image</label>
				<input class="form-control" type="text" name="image" value="" required="required" />
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">Price</label>
				<input class="form-control" type="text" name="price" value="" required="required" />
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">Description</label>
				<input class="form-control" type="text" name="description" value="" required="required" />
			</div>
		</div>
	
	<div class="col-lg-12">
		<div class="form-group" style="text-align: right;"><input class="btn btn-default" type="submit" value="Post FAQ" /></div>
	</div>
</form>

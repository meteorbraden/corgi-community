<div class="nav-justified">
        <div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
            <h3 class="c-font-bold c-font-uppercase">Recent Posts</h3>
            <div class="c-line-left c-theme-bg"></div>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="blog_recent_posts">
                <ul class="c-content-recent-posts-1">
                    <li>
                        <div class="c-image">
                            <img src="{theme_path}/assets/images/corgi-3.jpg" alt="" class="img-responsive"> </div>
                        <div class="c-post">
                            <a href="/blog/35-corgis-to-get-you-through-the-day" class="c-title">35 Corgis To Get You...</a>
                        </div>
                    </li>
                    <li>
                        <div class="c-image">
                            <img src="{theme_path}/assets/images/corgi-2.jpg" alt="" class="img-responsive"> </div>
                        <div class="c-post">
                            <a href="/blog/why-corgis-make-the-best-pets" class="c-title">Why Corgis Make The...</a>
                        </div>
                    </li>
                    <li>
                        <div class="c-image">
                            <img src="{theme_path}/assets/images/corgi-1.jpg" alt="" class="img-responsive"> </div>
                        <div class="c-post">
                            <a href="/blog/5-reasons-to-own-a-corgi" class="c-title">5 Reasons To Own A Corgi...</a>
                        </div>
                    </li>
                </ul>
            </div> 
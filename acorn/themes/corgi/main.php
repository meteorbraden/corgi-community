<!DOCTYPE html>
<html lang="en">
<!--[if IE 7]><html lang="en" class="ie7"><![endif]-->
<!--[if IE 8]><html lang="en" class="ie8"><![endif]-->
<!--[if IE 9]><html lang="en" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><html lang="en"><![endif]-->
<!--[if !IE]><html lang="en"><![endif]-->
<head>

	<!-- ==============================================
		Title and basic Meta Tags
	=============================================== -->
	<title>{title} | {site_name}</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="{keywords}" />
	<meta name="description" content="{description}" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	
	<!--
	<link rel="shortcut icon" type="image/x-icon" href="{theme_path}/images/icons/favicon.ico" />
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
	-->
	 
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&amp;subset=all' rel='stylesheet' type='text/css'>
        <link href="{theme_path}/assets/plugins/socicon/socicon.css" rel="stylesheet" type="text/css" />
        <link href="{theme_path}/assets/plugins/bootstrap-social/bootstrap-social.css" rel="stylesheet" type="text/css" />
        <link href="{theme_path}/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{theme_path}/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{theme_path}/assets/plugins/animate/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="{theme_path}/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN: BASE PLUGINS  -->
        <link href="{theme_path}/assets/plugins/revo-slider/css/settings.css" rel="stylesheet" type="text/css" />
        <link href="{theme_path}/assets/plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css" />
        <link href="{theme_path}/assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css" />
        <link href="{theme_path}/assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet" type="text/css" />
        <link href="{theme_path}/assets/plugins/owl-carousel/owl.transitions.css" rel="stylesheet" type="text/css" />
        <link href="{theme_path}/assets/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" />
        <link href="{theme_path}/assets/plugins/slider-for-bootstrap/css/slider.css" rel="stylesheet" type="text/css" />
        <!-- END: BASE PLUGINS -->
        <!-- BEGIN THEME STYLES -->
        <link href="{theme_path}/assets/base/css/plugins.css" rel="stylesheet" type="text/css" />
        <link href="{theme_path}/assets/base/css/components.css" id="style_components" rel="stylesheet" type="text/css" />
        <link href="{theme_path}/assets/base/css/themes/default.css" rel="stylesheet" id="style_theme" type="text/css" />
        <link href="{theme_path}/assets/base/css/custom.css" rel="stylesheet" type="text/css" />
        <!-- END THEME STYLES -->
        <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-72170498-1', 'auto');
		  ga('send', 'pageview');
		
		</script>

        <link rel="shortcut icon" href="{theme_path}/assets/favicon.ico" /> </head>

    <body class="c-layout-header-fixed c-layout-header-mobile-fixed">
	    <div class="preloader"></div>
        <!-- BEGIN: LAYOUT/HEADERS/HEADER-1 -->
        <!-- BEGIN: HEADER -->
        <header class="c-layout-header c-layout-header-4 c-layout-header-default-mobile" data-minimize-offset="80">
            <div class="c-navbar">
                <div class="container">
                    <!-- BEGIN: BRAND -->
                    <div class="c-navbar-wrapper clearfix">
                        <div class="c-brand c-pull-left">
                            <a href="/" class="c-logo">
                                <img src="{theme_path}/assets/base/img/layout/logos/logo-3.png" alt="corgi community, corgi blog" title="corgi blog, corgi community" class="c-desktop-logo logo-mobile">
                                <img src="{theme_path}/assets/base/img/layout/logos/logo-3.png" alt="corgi blog, corgi community" title="corgi community, corgi blog" class="c-desktop-logo-inverse logo-mobile">
                                <img src="{theme_path}/assets/base/img/layout/logos/logo-3.png" alt="corgi community, corgi blog" title="corgi blog, corgi community" class="c-mobile-logo logo-mobile"> </a>
                            <button class="c-hor-nav-toggler" type="button" data-target=".c-mega-menu">
                                <span class="c-line"></span>
                                <span class="c-line"></span>
                                <span class="c-line"></span>
                            </button>
                        </div>
                        <!-- END: BRAND -->
                        <!-- BEGIN: QUICK SEARCH -->
                        <form class="c-quick-search" action="#">
                            <input type="text" name="query" placeholder="Type to search..." value="" class="form-control" autocomplete="off">
                            <span class="c-theme-link">&times;</span>
                        </form>
                        <!-- END: QUICK SEARCH -->
                        <!-- BEGIN: HOR NAV -->
                        <!-- BEGIN: LAYOUT/HEADERS/MEGA-MENU -->
                        <!-- BEGIN: MEGA MENU -->
                        <!-- Dropdown menu toggle on mobile: c-toggler class can be applied to the link arrow or link itself depending on toggle mode -->
						
						{include:main_nav}
                    
                    </div>
                </div>
            </div>
        </header>
        
                                          
                                                   				
		{content}		
	
	<a name="footer"></a>
        <footer class="c-layout-footer c-layout-footer-6 c-bg-grey-1">
            <div class="container">          
             </div>
			</div>
		</div>
		<div class="c-postfooter c-bg-dark-2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 c-col">
                            <p class="c-copyright c-font-grey">2015 &copy; Corgi Community
                                <span class="c-font-grey-3">All Rights Reserved.</span>
                            </p>        
                        </div>
                    <div class="container">
						<div class="c-head">
							<div class="row">
                        		<div class="col-md-6">
									<div class="c-left">
										<div class="socicon">
	                                    <a href="https://www.facebook.com/corgicommunity" target="_blank" class="socicon-btn socicon-btn-circle socicon-solid c-bg-grey-1 c-font-grey-2 c-theme-on-hover socicon-facebook tooltips" data-original-title="Facebook" data-container="body"></a>
	                                    <a href="https://twitter.com/corgi_community" target="_blank" class="socicon-btn socicon-btn-circle socicon-solid c-bg-grey-1 c-font-grey-2 c-theme-on-hover socicon-twitter tooltips" data-original-title="Twitter" data-container="body"></a>
	                                    <a href="https://www.instagram.com/thecorgicommunity" target="_blank" class="socicon-btn socicon-btn-circle socicon-solid c-bg-grey-1 c-font-grey-2 c-theme-on-hover socicon-instagram tooltips" data-original-title="Instagram" data-container="body"></a>
                                </div>
                            </div>
                        </div>                          
                    </div>
                </div>
            </div>
        </footer>
        <!-- END: LAYOUT/FOOTERS/FOOTER-6 -->
        <!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
        <div class="c-layout-go2top">
            <i class="icon-arrow-up"></i>
        </div>
        <!-- END: LAYOUT/FOOTERS/GO2TOP -->
        <!-- BEGIN: LAYOUT/BASE/BOTTOM -->
        <!-- BEGIN: CORE PLUGINS -->
        <!--[if lt IE 9]>
	<script src="../assets/global/plugins/excanvas.min.js"></script> 
	<![endif]-->
        <script src="{theme_path}/assets/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="{theme_path}/assets/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="{theme_path}/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{theme_path}/assets/plugins/jquery.easing.min.js" type="text/javascript"></script>
        <script src="{theme_path}/assets/plugins/reveal-animate/wow.js" type="text/javascript"></script>
        <script src="{theme_path}/assets/base/js/scripts/reveal-animate/reveal-animate.js" type="text/javascript"></script>
        <!-- END: CORE PLUGINS -->
        <!-- BEGIN: LAYOUT PLUGINS -->
        <script src="{theme_path}/assets/plugins/revo-slider/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
        <script src="{theme_path}/assets/plugins/revo-slider/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
        <script src="{theme_path}/assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
        <script src="{theme_path}/assets/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
        <script src="{theme_path}/assets/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="{theme_path}/assets/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="{theme_path}/assets/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
        <script src="{theme_path}/assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
        <!-- END: LAYOUT PLUGINS -->
        <!-- BEGIN: THEME SCRIPTS -->
        <script src="{theme_path}/assets/base/js/components.js" type="text/javascript"></script>
        <script src="{theme_path}/assets/base/js/components-shop.js" type="text/javascript"></script>
        <script src="{theme_path}/assets/base/js/app.js" type="text/javascript"></script>
        <script>
            $(document).ready(function()
            {
                App.init(); // init core    
            });
        </script>
        <!-- END: THEME SCRIPTS -->
        <!-- BEGIN: PAGE SCRIPTS -->
        <script>
            $(document).ready(function()
            {
                var slider = $('.c-layout-revo-slider .tp-banner');
                var cont = $('.c-layout-revo-slider .tp-banner-container');
                var height = (App.getViewPort().width < App.getBreakpoint('md') ? 1024 : 620);
                var api = slider.show().revolution(
                {
                    delay: 15000,
                    startwidth: 1170,
                    startheight: height,
                    navigationType: "hide",
                    navigationArrows: "solo",
                    touchenabled: "on",
                    onHoverStop: "on",
                    keyboardNavigation: "off",
                    navigationStyle: "circle",
                    navigationHAlign: "center",
                    navigationVAlign: "center",
                    fullScreenAlignForce: "off",
                    shadow: 0,
                    fullWidth: "on",
                    fullScreen: "off",
                    spinner: "spinner2",
                    forceFullWidth: "on",
                    hideTimerBar: "on",
                    hideThumbsOnMobile: "on",
                    hideNavDelayOnMobile: 1500,
                    hideBulletsOnMobile: "on",
                    hideArrowsOnMobile: "on",
                    hideThumbsUnderResolution: 0,
                    videoJsPath: "rs-plugin/videojs/",
                });
            }); //ready
        </script>
        <!-- END: PAGE SCRIPTS -->
        <!-- END: LAYOUT/BASE/BOTTOM -->
    </body>

</html>
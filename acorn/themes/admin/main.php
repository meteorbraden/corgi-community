<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ACORN</title>
	
	<!-- Bootstrap -->
	<link href="{theme_path}/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	</head>
	<body>
	
		<nav class="navbar navbar-default">
			<div class="container">
			    <div class="navbar-header"><a class="navbar-brand" href="{admin_web_root}">{site_name}:</a></div>
			    
				{admin_nav}
				
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{username} <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="/<?php echo ACORN_LOGIN_ID; ?>?logout=1">Logout</a></li>
							<li><a href="/" target="_blank">Visit Website</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
		
		<div class="row">
			<div class="container">
				
				<div class="col-lg-12">
					<div class="page-header">
						<h1>{module_name} <small>{formatted_controller_name}</small></h1>
					</div>
				</div>
				
				<div class="col-lg-12" style="margin-bottom: 15px;">{module_nav}</div>
				
			</div>
		</div>
		
		<div class="row">
			<div class="container">
				
				<div class="col-lg-9">{controller}</div>
				<div class="col-lg-3">{controller_nav}</div>		
				
			</div>
		</div>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="{theme_path}/js/bootstrap.min.js"></script>
	</body>
</html>
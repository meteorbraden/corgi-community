<?php

	// Settup
	// -----
	$message = '';
	
	// (1) Check for logout
	// --------------------
	if(isset($_GET['logout']))
	{
		unset($_SESSION['acorn']['user']);
		$message = '<div class="alert alert-success" role="alert">You\'re outta here!</div>';
	}
	
	// (2) Check if already logged in
	// ------------------------------
	if(isset($_SESSION['acorn']['user']))
	{
		header('Location: http://' . $_SERVER['HTTP_HOST'] . '/' . ACORN_ADMIN_ID);
		exit();
	}
	
	// (3) Check for login
	// -------------------
	if(isset($_POST['username']) && isset($_POST['password']))
	{
		include_module('users');
		
		// Check login
		if(users_login($_POST['username'], $_POST['password']))
		{
			header('Location: http://' . $_SERVER['HTTP_HOST'] . '/' . ACORN_ADMIN_ID);
			exit();
		}
		else
			$message = '<div class="alert alert-danger" role="alert">Well that didn\'t work</div>';
	}
	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Login</title>
		
		<!-- Bootstrap -->
		<link href="<?php echo ACORN_THEME_WEB_PATH; ?>/admin/css/bootstrap.min.css" rel="stylesheet">
	
		
	</head>
	<body>
		
		<div style="width: 20%; margin: 20% auto;">
			<?php echo $message; ?>
			<form method="post" action="/<?php echo ACORN_LOGIN_ID; ?>">
				<div class="form-group"><input class="form-control" type="text" name="username" placeholder="Username" required="required" /></div>
				<div class="form-group"><input class="form-control" type="password" name="password" placeholder="Password" required="required" /></div>
				<div class="form-group"><input class="btn btn-default" type="submit" value="Login" /></div>
			</form>
		</div>
	
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="<?php echo ACORN_THEME_WEB_PATH; ?>/admin/js/bootstrap.min.js"></script>
	</body>
</html>
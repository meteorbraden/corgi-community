<?php
	
	session_start();
	
	// (1) Define Globals
	// --------------------
	define("IN_DEVELOPMENT", true);
	define("ACORN_DIR", '/acorn');
	define("ACORN_PATH", $_SERVER['DOCUMENT_ROOT'] . ACORN_DIR);
	define("ACORN_FRAMEWORK_PATH", ACORN_PATH . '/framework');
	define("ACORN_CONTENT_PATH", ACORN_PATH . '/content');
	define("ACORN_MODULE_PATH", ACORN_PATH . '/modules');
	define("ACORN_MODULE_WEB_PATH", ACORN_DIR . '/modules');
	define("ACORN_THEME_PATH", ACORN_PATH . '/themes');
	define("ACORN_THEME_WEB_PATH", ACORN_DIR . '/themes');
	define("ACORN_ADMIN_ID", 'admin');
	define("ACORN_LOGIN_ID", 'admin-login');
	define("ACORN_DEPTH_ID", '_');
	define("ACORN_INVISIBLE_FILES", ".,..,.htaccess,.htpasswd,.DS_Store,_notes,.git,.svn");
	define("ACORN_URI", trim(strtok($_SERVER['REQUEST_URI'], '?'), '/'));
	
	
	// (2) Include site specific settings
	// ------------------------------------
	if(file_exists(ACORN_FRAMEWORK_PATH . '/settings.php'))
		include(ACORN_FRAMEWORK_PATH . '/settings.php');
	
		
	// (3) Connect to database and site settings
	// -------------------------------------------
	if(file_exists(ACORN_FRAMEWORK_PATH . '/data.php'))
	{
		include(ACORN_FRAMEWORK_PATH . '/data.php');
		
		// Set site settings
		if((IN_DEVELOPMENT || !isset($_SESSION['acorn']['site_settings'])) && function_exists('get_site_settings'))
		{
			$site_settings = get_site_settings();
			
			foreach($site_settings as $i => $v)
				$_SESSION['acorn']['site_settings'][$v['name']] = $v['value'];
		}
	}
	
	
	// (3) Error handling
	// --------------------
	if(IN_DEVELOPMENT)
	{
		ini_set('display_errors', 1);
		error_reporting(E_ALL);
	}
	else
		error_reporting(0);
	

	
/* //////////////////////////////////////////////////////////////////////////////////////////////////////////// */
/* /// Functions ////////////////////////////////////////////////////////////////////////////////////////////// */
/* //////////////////////////////////////////////////////////////////////////////////////////////////////////// */

	
	// (x) Include Module Functions
	// ----------------------------
	function include_module($module)
	{
		if(file_exists(ACORN_MODULE_PATH . '/' . $module . '/functions.php'))
			include(ACORN_MODULE_PATH . '/' . $module . '/functions.php');
		else
			echo $module . ' functions not found.';
	}
	
?>